SELECT C.Name,
  CASE WHEN LOCATE('@mail.ru', C.Email) > 0 THEN 0 ELSE COUNT(O.Id) END AS Count
FROM Clients C
LEFT JOIN Orders O ON C.Id = O.Clients_id
LEFT JOIN Products P ON O.Id = P.Order_id
WHERE P.Id IN (151515,151617,151514)
  AND DATE_FORMAT(FROM_UNIXTIME(O.Ctime), '%m%Y') = '032015'
GROUP BY C.Name
ORDER BY (P.Price * P.Count) DESC;
