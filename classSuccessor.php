<?php include("classBase.php");?>

<?php
    
    class Successor extends Base
    {
        /**
         * обратная замена, массив сопоставлений
         *
         * @param  array  $arReplaces
         * @return string
         */
        public function reverseReplace(array $arReplaces = [])
        {
            $prepareReplace = $this->prepareReplace($arReplaces);

            return parent::directReplace($prepareReplace);
        }

        /**
         * обратная замена, ключ-значение
         *
         * @param  string $key
         * @param  string $val
         * @return string
         */
        public function reverseReplaceKeyVal($key, $val)
        {
            return parent::directReplaceKeyVal($val, $key);
        }

        /**
         * подготовка массива для обратной замены
         *
         * @param  array  $arRreplaces
         * @return array
         */
        protected function prepareReplace(array $arRreplaces = [])
        {
            return array_reverse(array_flip($arRreplaces));
        }
    }
?>
