<?php

    class Base {

        public $url;
        static private $_instance = null;

        public function __construct($url = '')
        {
            if (!empty($url)) {
                $this->url = $url;
            }
        }

        /**
         * прямая замена, массив сопоставлений
         *
         * @param array $arReplaces
         * @return mixed|null
         */
        public function directReplace(array $arReplaces = [])
        {
            $key = array_keys($arReplaces);
            $val = array_values($arReplaces);

            while(true)
            {
                $content = $this->getContent();
                self::$_instance = str_replace($key, $val, $content);

                if (self::$_instance === $content) break;
            }

            return self::$_instance;
        }

        /**
         * прямая замена, ключ-значение
         *
         * @param  string $key
         * @param  string $val
         * @return string
         */
        public function directReplaceKeyVal($key, $val)
        {
            while(true)
            {
                $content = $this->getContent();
                self::$_instance = str_replace($key, $val, $content);

                if (self::$_instance === $content) break;
            }

            return self::$_instance;
        }

        public function getUrl()
        {
            return $this->url;
        }

        public function setUrl($url)
        {
            $this->url = $url;
        }

        public function getContent($timeout = 10)
        {
            if (is_null(self::$_instance)) {
                self::$_instance = $this->connect($timeout);
            }

            return self::$_instance;
        }

        protected function connect($timeout)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            $result = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);

            if (! isset($info['http_code']) || $info['http_code'] !== 200) {
                throw new \Exception('Не удалось получить данные с сайта '.$this->url);
            }

            return $result;
        }
    }
?>